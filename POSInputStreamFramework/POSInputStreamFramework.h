//
//  POSInputStreamFramework.h
//  POSInputStreamFramework
//
//  Created by Brian Kauke on 5/1/18.
//  Copyright © 2018 Pavel Osipov. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for POSInputStreamFramework.
FOUNDATION_EXPORT double POSInputStreamFrameworkVersionNumber;

//! Project version string for POSInputStreamFramework.
FOUNDATION_EXPORT const unsigned char POSInputStreamFrameworkVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <POSInputStreamFramework/PublicHeader.h>
#import "POSBlobInputStream.h"
#import "POSBlobInputStreamDataSource.h"

